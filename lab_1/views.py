from django.shortcuts import render
from datetime import date
# Enter your name here
mhs_name = 'Priambudi Lintang Bagaskara' # TODO Implement this
born_year = 1998
# Create your views here.
def index(request):
    response = {'name' : mhs_name, "age" : calculate_age(born_year)}
    return render(request, 'index.html', response)

# TODO Implement this to complete last checklist
def calculate_age(birth_year):
    #today = date.today()
    #return today.year - birth_year.year - ((today.month, today.day) < (birth_year.month, birth_year.day))
    return date.today().year - birth_year
